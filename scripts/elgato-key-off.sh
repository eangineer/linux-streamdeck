#!/bin/bash

# Turn on Left Elgato Key Light Air
curl --location --request PUT 'http://10.0.0.23:9123/elgato/lights' \
--header 'Content-Type: application/json' \
--data-raw '{"lights":[{"on":0}],"numberOfLights":1}'

# Turn on Right Elgato Key Light Air
curl --location --request PUT 'http://10.0.0.15:9123/elgato/lights' \
--header 'Content-Type: application/json' \
--data-raw '{"lights":[{"on":0}],"numberOfLights":1}'