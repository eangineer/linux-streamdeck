#!/bin/bash

# Get on/off status of a specific light. Outputs 0 (Off) or 1 (on) for evaluation in next step
curl --silent --location --request GET 'http://10.0.0.23:9123/elgato/lights' \
--header 'Content-Type: application/json' | grep -o -e 'on":0' > /dev/null

# If status = off, turn on. If on, the turn off.
if [ $? -eq 0 ] ; then curl --silent --location --request PUT 'http://10.0.0.23:9123/elgato/lights' \
--header 'Content-Type: application/json' --data-raw '{"lights":[{"on":1}],"numberOfLights":1}' ; \
else curl --silent --location --request PUT 'http://10.0.0.23:9123/elgato/lights' \
--header 'Content-Type: application/json' \
--data-raw '{"lights":[{"on":0}],"numberOfLights":1}' ; fi

curl --silent --location --request GET 'http://10.0.0.15:9123/elgato/lights' \
--header 'Content-Type: application/json' | grep -o -e 'on":0' > /dev/null
if [ $? -eq 0 ] ; then curl --silent --location --request PUT 'http://10.0.0.15:9123/elgato/lights' \
--header 'Content-Type: application/json' --data-raw '{"lights":[{"on":1}],"numberOfLights":1}' ; \
else curl --silent --location --request PUT 'http://10.0.0.15:9123/elgato/lights' \
--header 'Content-Type: application/json' \
--data-raw '{"lights":[{"on":0}],"numberOfLights":1}' ; fi