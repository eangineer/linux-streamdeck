# Linux-Streamdeck

## Useful information regarding setting up an Elgato Stream Deck on Linux.

### Waiting on release of [Boatswain](https://gitlab.gnome.org/World/boatswain)

Elgato's Stream Deck is an amazing tool, not just for streamers, but for anyone who likes to have dedicated keys for functionality. Unfortunately, Elgato does NOT support Linux (BOOOO!).

On the other hand, fortunately, the Linux community is a fairly crafty and industrious group as a whole. There is a utility, called streamdeck-ui, which allows us to use the Stream Deck on Linux. Obviously, the level of capability and integration with third party products/software that Elgato provides on Windows/Mac is not present. However, with a bit of Bash scripting and some Google-Fu, there are work-arounds for many of these issues. Below are some of the things I have figured out how to do with my Stream Deck.

## Installation
* Repository:
    [streamdeck-ui](https://github.com/timothycrosley/streamdeck-ui)
* Add to autostart/startup (must always be running):
    ```
    streamdeck &
    ```

## Launching applications
* Launching Brave in "app" mode for a given URL:
    ``` bash
    brave-browser --app="URL" 
    ```
    * Example: 
    ``` bash
    brave-browser --app="https://www.gitlab.com"
    ```

    
## Audio control with Pulse Audio
* List all of the available sinks (output devices):
    ``` bash
    pactl list sinks
    ```

    **NOTE:** Always use full name of the sink, NOT the index/ID #! The sequencing of device numbers is NOT consistent between reboots & hardware changes.

* Set default sink (ouput):
    ``` bash
    pactl set-default-sink SINK-NAME
    ```
    * EXAMPLE:

        **NOTE**: My speakers are connected to the following sink: alsa_output.pci-0000_0c_00.4.analog-stereo
        ``` bash
        pactl set-default-sink alsa_output.pci-0000_0c_00.4.analog-stereo
        ```

    * EXAMPLE (Switching to Headset audio output):

        **NOTE:** My Corsair wireless headset is: alsa_output.usb-Corsair_Corsair_VOID_Wireless_Gaming_Dongle-00.analog-stereo
        ``` bash
        pactl set-default-sink alsa_output.usb-Corsair_Corsair_VOID_Wireless_Gaming_Dongle-00.analog-stereo
        ```

    **NOTE:** By using both of these, I can switch between audio outputs with the press of a button!
* The following commands are now available as scripts in /scripts. When testing on a new Stream Deck Mini, for some reason the commands were not working, but do work when run as a script.

* Increase volume by 5%:
    ``` bash
    pactl set-sink-volume $(pactl get-default-sink) +5%
    ```
* Decrease volume by 5%:
    ``` bash
    pactl set-sink-volume $(pactl get-default-sink) -5%
    ```
* Mute output muting:
    ``` bash
    pactl set-sink-mute $(pactl get-default-sink) toggle
    ```

## Controlling OBS Studio:

- [ ] Work in progress

    **NOTE:** Requires [xdotool](https://linuxcommandlibrary.com/man/xdotool)

## Controlling music.apple.com current playlist/channel:

**NOTE:** Requires [xdotool](https://linuxcommandlibrary.com/man/xdotool)

* Create scripts based on the below snippets and add as commands on streamdeck-ui.

* Launching as an "app":
    **NOTE:** This may also work on chromium based browsers, but not tested)
    ``` bash
    brave-browser --app="https://music.apple.com"
    ```
* Pause playback, and return to original window (assumes only window open with "Apple" in title):
    ``` bash
    #! /bin/bash

    CURRENT_WINDOW_ID=$(xdotool getactivewindow)
    xdotool getactivewindow search "Apple" windowactivate --sync key space windowactivate $CURRENT_WINDOW_ID

    ```
* Next track, and return to original window:
    ``` bash
    #! /bin/bash

    CURRENT_WINDOW_ID=$(xdotool getactivewindow)
    xdotool getactivewindow search "Apple" windowactivate --sync key Shift+Ctrl+Right windowactivate $CURRENT_WINDOW_ID

    ```
* Previous track, and return to original window:
    ``` bash
    #! /bin/bash

    CURRENT_WINDOW_ID=$(xdotool getactivewindow)
    xdotool getactivewindow search "Apple" windowactivate --sync key Shift+Ctrl+Left windowactivate $CURRENT_WINDOW_ID

    ```

## Controlling Elgato Key Light Air (on/off):
* Turn a light **ON**** (You can get the ip from control center app):
    ``` bash
    #!/bin/bash

    curl --location --request PUT 'http://<IP_OF_LIGHT>:9123/elgato/lights' \
    --header 'Content-Type: application/json' \
    --data-raw '{"lights":[{"on":1}],"numberOfLights":1}'
    ```
* Turn a light **OFF** (You can get the ip from control center app):
    ``` bash
    #!/bin/bash

    curl --location --request PUT 'http://<IP_OF_LIGHT>:9123/elgato/lights' \
    --header 'Content-Type: application/json' \
    --data-raw '{"lights":[{"on":0}],"numberOfLights":1}'
    ```
* Toggling a light on or off depending on state:
    ``` bash
    #!/bin/bash

    # Get on/off status of a specific light. Outputs 0 (Off) or 1 (on) for evaluation in next step
    curl --silent --location --request GET 'http://<IP_OF_LIGHT>/elgato/lights' \
    --header 'Content-Type: application/json' | grep -o -e 'on":0' > /dev/null

    # If status = off, turn on. If on, the turn off.
    if [ $? -eq 0 ] ; then curl --silent --location --request PUT 'http://<IP_OF_LIGHT>:9123/elgato/lights' \
    --header 'Content-Type: application/json' --data-raw '{"lights":[{"on":1}],"numberOfLights":1}' ; \
    else curl --silent --location --request PUT 'http://<IP_OF_LIGHT>:9123/elgato/lights' \
    --header 'Content-Type: application/json' \
    --data-raw '{"lights":[{"on":0}],"numberOfLights":1}' ; fi
    ```
    **NOTE:** Multiple curl commands to the different IP's will allow control of multiple devices simultaneously, example can be found in the ./scripts folder.
    - [ ] TODO: mDNS discovery of IP's.
        ``` bash
        avahi-browse --all --resolve --parsable --terminate | grep "elgato-key-light-air" | uniq -u | awk -F";" '{print $8}'

        avahi-browse -rpt _elg._tcp | grep "IPv4" | grep "elgato-key-light-air-" | awk -F";" '{print $8}' | uniq -u
        ```
    - [x] TODO: enable toggle by reading current state and executing opposite.
